#!/usr/bin/env python

import os
import urllib
import urllib2
import json

import jinja2
import webapp2

key = "trnsl.1.1.20150127T220525Z.fb42a76b2a2f1e97.a1ff94303180b250c21156e50dad56e2fe2d6e29"

def sanit(text):
	s = str()
	for i in text:
		i = ord(i)
		if (i>=65 and i<= 90) or (i>=97 and i<=122) or (i==44) or (i==46):
			s += chr(i)
		elif (i==195) or (i==129) or (i==137) or (i==141) or (i==145) or (i==147) or (i==154) or (i==169) or (i==173) or (i==177) or (i==179) or (i==186):
			s += chr(i)
		elif i==32:
			s += '+'
	return s

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
	
class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render())

class SubHandler(webapp2.RequestHandler):
    def get(self):
        sub = self.request.get("sub")
        fr = self.request.get("from")
        to = self.request.get("to")
        response = urllib2.urlopen( "https://www.reddit.com/r/" + sub + ".json" ).read()
	data = json.loads(response)
        stories = []
        for story in data["data"]["children"]:
            url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+key+"&lang="+fr+"-"+to+"&text="+sanit(story["data"]["title"])
            response = urllib2.urlopen( url ).read()
            data = json.loads(response)
	    title = data["text"][0]
            stories.append( [ story["data"]["id"], title, story["data"]["num_comments"], story["data"]["url"] ] )
        #for n in stories:
        #    print(n[0])
        template_values = { 'stories': stories, 'sub': sub, 'from': fr, 'to': to}
        template = JINJA_ENVIRONMENT.get_template('subreddit.html')
        self.response.write(template.render(template_values))

class ComHandler(webapp2.RequestHandler):
    def get(self):
        #https://www.reddit.com/r/worldnews/comments/2vnbwm.json
        fr = self.request.get("from")
        to = self.request.get("to")
        response = urllib2.urlopen( "https://www.reddit.com/r/" + self.request.get("sub") + "/comments/" + self.request.get("story") + ".json" ).read()
        data = json.loads(response)
        comments = []
        for y in data:
            for x in y["data"]["children"]:
                try:
                    url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+key+"&lang="+fr+"-"+to+"&text="+sanit(x["data"]["body"])
                    print(url)
                    response = urllib2.urlopen( url ).read()
                    data = json.loads(response)
                    body = data["text"][0]
                    comments.append([ body, x["data"]["author"] ])
                except:
                    print "er"
        template_values = { 'comments': comments }
        template = JINJA_ENVIRONMENT.get_template('comments.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/topic', SubHandler),
    ('/comments', ComHandler)
], debug=True)
